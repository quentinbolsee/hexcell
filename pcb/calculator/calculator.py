def main():
    resistors = [30,
                 33,
                 36,
                 39,
                 43,
                 47,
                 49.9,
                 51,
                 56,
                 62,
                 68,
                 75,
                 82,
                 91,
                 100,
                 110,
                 120,
                 130,
                 150,
                 180,
                 200,
                 220,
                 240,
                 270,
                 300,
                 330,
                 360,
                 390,
                 430,
                 470,
                 499,
                 510,
                 560,
                 620,
                 680,
                 750,
                 820,
                 910,
                 1000,
                 1200,
                 1500,
                 2000,
                 2200,
                 2700,
                 3300,
                 4700,
                 4990,
                 5600,
                 10000]

    target_ratio = 4.5

    ratio_dict = {}

    n = len(resistors)

    for i in range(n):
        for j in range(n):
            if j < i:
                continue
            ratio_dict[(i, j)] = resistors[j] / resistors[i]

    ij_sorted = sorted(ratio_dict.keys(), key=lambda x: abs(ratio_dict[x] - target_ratio))

    i, j = ij_sorted[0]
    print(f"Values: {resistors[i]}, {resistors[j]}")
    print(f"Ratio: {ratio_dict[i, j]}")
    print(f"Error: {100 * abs(ratio_dict[i, j] / target_ratio - 1):.2f} %")


if __name__ == "__main__":
    main()
